package com.chattrawut.week4_plusgame2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnGame1 = findViewById<Button>(R.id.btnGame1)
        val btnGame2 = findViewById<Button>(R.id.btnGame2)
        val btnGame3 = findViewById<Button>(R.id.btnGame3)
        btnGame1.setOnClickListener {
            val intent = Intent(MainActivity@this, Game::class.java)
            intent.putExtra("selectGame", "1");
            startActivity(intent)
        }
        btnGame2.setOnClickListener {
            val intent = Intent(MainActivity@this, Game::class.java)
            intent.putExtra("selectGame", "2");
            startActivity(intent)
        }
        btnGame3.setOnClickListener {
            val intent = Intent(MainActivity@this, Game::class.java)
            intent.putExtra("selectGame", "3");
            startActivity(intent)
        }
    }
}


