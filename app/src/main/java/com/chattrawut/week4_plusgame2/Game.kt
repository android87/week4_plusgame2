package com.chattrawut.week4_plusgame2

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_game.*
import kotlin.math.log

public var correct: Int = 0
public var incorrect: Int = 0
public var isSelectAnswer = false

class Game : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        val intent = intent
        if (intent.hasExtra("selectGame")) {
            val selectGame = intent.getStringExtra("selectGame")?.toString()
            play(selectGame)
        }
    }

    private fun play(selectGame: String?) {
        val txtNum1 = findViewById<TextView>(R.id.txtNum1)
        val txtNum2 = findViewById<TextView>(R.id.txtNum2)
        val txtAnswer = findViewById<TextView>(R.id.txtAnswer)
        val txtOperator = findViewById<TextView>(R.id.txtOperator)

        when (selectGame) {
            "1" -> {
                txtOperator.text = "+"
            }
            "2" -> {
                txtOperator.text = "-"
            }
            else -> {
                txtOperator.text = "x"
            }
        }

        txtAnswer.text = ""

        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val btnPlay = findViewById<Button>(R.id.btnPlay)
        btnPlay.visibility = View.GONE

        btn1.setBackgroundColor(Color.WHITE)
        btn2.setBackgroundColor(Color.WHITE)
        btn3.setBackgroundColor(Color.WHITE)

        isSelectAnswer = false

        txtNum1.text = (1..10).random().toString()
        txtNum2.text = (1..10).random().toString()

        val randomBtn = (1..3).random().toString()
        var num1 = (txtNum1).text.toString().toInt()
        var num2 = (txtNum2).text.toString().toInt()


        var randomNum1: String
        var randomNum2: String
        var randomNum3: String

        when (selectGame) {
            "1" -> {
                randomNum1 = ((num1 +num2).toString())
                do {
                    randomNum2 = (1..20).random().toString()
                } while (randomNum2 == (num1 + num2).toString())
                do {
                    randomNum3 = (1..20).random().toString()
                } while (randomNum3 == (num1 + num2).toString() || randomNum3 == randomNum2)
            }
            "2" -> {
                randomNum1 = ((num1 - num2).toString())
                do {
                    randomNum2 = (1..10).random().toString()
                } while (randomNum2 == (num1 - num2).toString())
                do {
                    randomNum3 = (1..10).random().toString()
                } while (randomNum3 == (num1 - num2).toString() || randomNum3 == randomNum2)
            }
            else -> {
                randomNum1 = ((num1 * num2).toString())
                do {
                    randomNum2 = (1..100).random().toString()
                } while (randomNum2 == (num1 * num2).toString())
                do {
                    randomNum3 = (1..100).random().toString()
                } while (randomNum3 == (num1 * num2).toString() || randomNum3 == randomNum2)
            }
        }

        when (randomBtn) {
            "1" -> {
                btn1.text = (randomNum1.toInt()).toString()
                btn2.text = (randomNum2.toInt()).toString()
                btn3.text = (randomNum3.toInt()).toString()
            }
            "2" -> {
                btn2.text = (randomNum1.toInt()).toString()
                btn1.text = (randomNum2.toInt()).toString()
                btn3.text = (randomNum3.toInt()).toString()
        }
            else -> {
                btn3.text = (randomNum1.toInt()).toString()
                btn1.text = (randomNum2.toInt()).toString()
                btn2.text = (randomNum3.toInt()).toString()
            }
        }

        btn1.setOnClickListener {
            if (randomBtn == "1") {
                showWin(btn1)
            } else {
                showLose(btn1)
            }
        }
        btn2.setOnClickListener {
            if (randomBtn == "2") {
                showWin(btn2)
            } else {
                showLose(btn2)
            }
        }
        btn3.setOnClickListener {
            if (randomBtn == "3") {
                showWin(btn3)
            } else {
                showLose(btn3)
            }
        }
        btnPlay.setOnClickListener {
            play(selectGame)
        }
    }

    private fun showLose(
        btn: Button
    ) {
        if(!isSelectAnswer){
            btn.setBackgroundColor(Color.RED)
            txtAnswer.text = "You're wrong"
            incorrect++
            txtStatic.text = "correct:${correct} incorrect:${incorrect}"
            btnPlay.visibility = View.VISIBLE
            isSelectAnswer =  true


        }

    }
    private fun showWin(
        btn: Button
    ) {
        if(!isSelectAnswer){
            btn.setBackgroundColor(Color.GREEN)
            txtAnswer.text = "You're right"
            correct++
            txtStatic.text = "correct:${correct} incorrect:${incorrect}"
            btnPlay.visibility = View.VISIBLE
            isSelectAnswer =  true
        }

    }
}
